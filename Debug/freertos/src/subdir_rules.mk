################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Each subdirectory must supply rules for building sources it contributes
freertos/src/croutine.obj: C:/Users/Ivar/Dropbox\ (Personal)/UOC/Sistemes\ Encastats/Workspace/freertos/src/croutine.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP432 Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/Ivar/Dropbox (Personal)/UOC/Sistemes Encastats/Workspace/freertos/inc" --include_path="C:/Users/Ivar/Dropbox (Personal)/UOC/Sistemes Encastats/Workspace/PAC2/sensors" --include_path="C:/Users/Ivar/Dropbox (Personal)/UOC/Sistemes Encastats/Workspace/freertos/cortex-m4" --include_path="C:/Users/Ivar/Dropbox (Personal)/UOC/Sistemes Encastats/Workspace/ti/msp432" --include_path="C:/Users/Ivar/Dropbox (Personal)/UOC/Sistemes Encastats/Workspace/PAC2" --include_path="C:/ti/ccsv7/ccs_base/arm/include" --include_path="C:/ti/ccsv7/ccs_base/arm/include/CMSIS" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --advice:power="all" --define=__MSP432P401R__ --define=TARGET_IS_MSP432P4XX --define=ccs -g --c99 --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="freertos/src/croutine.d_raw" --obj_directory="freertos/src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

freertos/src/event_groups.obj: C:/Users/Ivar/Dropbox\ (Personal)/UOC/Sistemes\ Encastats/Workspace/freertos/src/event_groups.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP432 Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/Ivar/Dropbox (Personal)/UOC/Sistemes Encastats/Workspace/freertos/inc" --include_path="C:/Users/Ivar/Dropbox (Personal)/UOC/Sistemes Encastats/Workspace/PAC2/sensors" --include_path="C:/Users/Ivar/Dropbox (Personal)/UOC/Sistemes Encastats/Workspace/freertos/cortex-m4" --include_path="C:/Users/Ivar/Dropbox (Personal)/UOC/Sistemes Encastats/Workspace/ti/msp432" --include_path="C:/Users/Ivar/Dropbox (Personal)/UOC/Sistemes Encastats/Workspace/PAC2" --include_path="C:/ti/ccsv7/ccs_base/arm/include" --include_path="C:/ti/ccsv7/ccs_base/arm/include/CMSIS" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --advice:power="all" --define=__MSP432P401R__ --define=TARGET_IS_MSP432P4XX --define=ccs -g --c99 --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="freertos/src/event_groups.d_raw" --obj_directory="freertos/src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

freertos/src/heap_2.obj: C:/Users/Ivar/Dropbox\ (Personal)/UOC/Sistemes\ Encastats/Workspace/freertos/src/heap_2.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP432 Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/Ivar/Dropbox (Personal)/UOC/Sistemes Encastats/Workspace/freertos/inc" --include_path="C:/Users/Ivar/Dropbox (Personal)/UOC/Sistemes Encastats/Workspace/PAC2/sensors" --include_path="C:/Users/Ivar/Dropbox (Personal)/UOC/Sistemes Encastats/Workspace/freertos/cortex-m4" --include_path="C:/Users/Ivar/Dropbox (Personal)/UOC/Sistemes Encastats/Workspace/ti/msp432" --include_path="C:/Users/Ivar/Dropbox (Personal)/UOC/Sistemes Encastats/Workspace/PAC2" --include_path="C:/ti/ccsv7/ccs_base/arm/include" --include_path="C:/ti/ccsv7/ccs_base/arm/include/CMSIS" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --advice:power="all" --define=__MSP432P401R__ --define=TARGET_IS_MSP432P4XX --define=ccs -g --c99 --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="freertos/src/heap_2.d_raw" --obj_directory="freertos/src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

freertos/src/list.obj: C:/Users/Ivar/Dropbox\ (Personal)/UOC/Sistemes\ Encastats/Workspace/freertos/src/list.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP432 Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/Ivar/Dropbox (Personal)/UOC/Sistemes Encastats/Workspace/freertos/inc" --include_path="C:/Users/Ivar/Dropbox (Personal)/UOC/Sistemes Encastats/Workspace/PAC2/sensors" --include_path="C:/Users/Ivar/Dropbox (Personal)/UOC/Sistemes Encastats/Workspace/freertos/cortex-m4" --include_path="C:/Users/Ivar/Dropbox (Personal)/UOC/Sistemes Encastats/Workspace/ti/msp432" --include_path="C:/Users/Ivar/Dropbox (Personal)/UOC/Sistemes Encastats/Workspace/PAC2" --include_path="C:/ti/ccsv7/ccs_base/arm/include" --include_path="C:/ti/ccsv7/ccs_base/arm/include/CMSIS" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --advice:power="all" --define=__MSP432P401R__ --define=TARGET_IS_MSP432P4XX --define=ccs -g --c99 --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="freertos/src/list.d_raw" --obj_directory="freertos/src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

freertos/src/queue.obj: C:/Users/Ivar/Dropbox\ (Personal)/UOC/Sistemes\ Encastats/Workspace/freertos/src/queue.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP432 Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/Ivar/Dropbox (Personal)/UOC/Sistemes Encastats/Workspace/freertos/inc" --include_path="C:/Users/Ivar/Dropbox (Personal)/UOC/Sistemes Encastats/Workspace/PAC2/sensors" --include_path="C:/Users/Ivar/Dropbox (Personal)/UOC/Sistemes Encastats/Workspace/freertos/cortex-m4" --include_path="C:/Users/Ivar/Dropbox (Personal)/UOC/Sistemes Encastats/Workspace/ti/msp432" --include_path="C:/Users/Ivar/Dropbox (Personal)/UOC/Sistemes Encastats/Workspace/PAC2" --include_path="C:/ti/ccsv7/ccs_base/arm/include" --include_path="C:/ti/ccsv7/ccs_base/arm/include/CMSIS" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --advice:power="all" --define=__MSP432P401R__ --define=TARGET_IS_MSP432P4XX --define=ccs -g --c99 --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="freertos/src/queue.d_raw" --obj_directory="freertos/src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

freertos/src/tasks.obj: C:/Users/Ivar/Dropbox\ (Personal)/UOC/Sistemes\ Encastats/Workspace/freertos/src/tasks.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP432 Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/Ivar/Dropbox (Personal)/UOC/Sistemes Encastats/Workspace/freertos/inc" --include_path="C:/Users/Ivar/Dropbox (Personal)/UOC/Sistemes Encastats/Workspace/PAC2/sensors" --include_path="C:/Users/Ivar/Dropbox (Personal)/UOC/Sistemes Encastats/Workspace/freertos/cortex-m4" --include_path="C:/Users/Ivar/Dropbox (Personal)/UOC/Sistemes Encastats/Workspace/ti/msp432" --include_path="C:/Users/Ivar/Dropbox (Personal)/UOC/Sistemes Encastats/Workspace/PAC2" --include_path="C:/ti/ccsv7/ccs_base/arm/include" --include_path="C:/ti/ccsv7/ccs_base/arm/include/CMSIS" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --advice:power="all" --define=__MSP432P401R__ --define=TARGET_IS_MSP432P4XX --define=ccs -g --c99 --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="freertos/src/tasks.d_raw" --obj_directory="freertos/src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

freertos/src/timers.obj: C:/Users/Ivar/Dropbox\ (Personal)/UOC/Sistemes\ Encastats/Workspace/freertos/src/timers.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP432 Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=FPv4SPD16 -me --include_path="C:/Users/Ivar/Dropbox (Personal)/UOC/Sistemes Encastats/Workspace/freertos/inc" --include_path="C:/Users/Ivar/Dropbox (Personal)/UOC/Sistemes Encastats/Workspace/PAC2/sensors" --include_path="C:/Users/Ivar/Dropbox (Personal)/UOC/Sistemes Encastats/Workspace/freertos/cortex-m4" --include_path="C:/Users/Ivar/Dropbox (Personal)/UOC/Sistemes Encastats/Workspace/ti/msp432" --include_path="C:/Users/Ivar/Dropbox (Personal)/UOC/Sistemes Encastats/Workspace/PAC2" --include_path="C:/ti/ccsv7/ccs_base/arm/include" --include_path="C:/ti/ccsv7/ccs_base/arm/include/CMSIS" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.4.LTS/include" --advice:power="all" --define=__MSP432P401R__ --define=TARGET_IS_MSP432P4XX --define=ccs -g --c99 --gcc --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="freertos/src/timers.d_raw" --obj_directory="freertos/src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '


